import java.io.File
import kotlin.io.path.*
import java.nio.file.Path
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Scanner
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.readLines

const val redColor = "\u001B[31m"
const val reset = "\u001B[0m"
const val greenColor = "\u001B[32m"
val newDataPath = Path("src/main/Data/UserData")
val scanner = Scanner(System.`in`)
fun main(){

    importFiles()
    println("""
      ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚
❚█══█❚         _____           _____________      ______        ______           _____          ❚█══█❚
❚█══█❚        /     \         |   __________|    |      \      /      |         /     \         ❚█══█❚
❚█══█❚       /   _   \        |   |              |       \    /       |        /   _   \        ❚█══█❚
❚█══█❚      /   /_\   \       |   ----------     |        \__/        |       /   /_\   \       ❚█══█❚
❚█══█❚     /   _____   \      |__________   |    |     |\______/|     |      /   _____   \      ❚█══█❚
❚█══█❚    /   /     \   \      _________|   |    |     |        |     |     /   /     \   \     ❚█══█❚       
❚█══█❚   /___/       \___\    |_____________|    |_____|        |_____|    /___/       \___\    ❚█══█❚
      ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚ ❚█══█❚  
    """)

    do{

        gymMenu()
        print("Escull una opció: ")
        val userNumber = scanner.nextInt()

        when(userNumber){
            1 -> {
                print("Introdueix nom i cognom: ")
                val name = readLine()!!
                print("Introdueix el número de telèfon: ")
                val number = readLine()!!
                print("Introdueix el correu electrònic: ")
                val email = readLine()!!
                println(createUser(name,number,email))
                }

            2 -> {
                print("Introdueix l'ID de l'usuari: ")
                val idUser = readLine()!!
                print("Introdueix nom i cognom nous: ")
                val modifyName = readLine()!!
                print("Introdueix el número de telèfon nou: ")
                val modifyNumber = readLine()!!
                print("Introdueix el correu electrònic nou: ")
                val mosifyEmail = readLine()!!

               println(modifyUser(idUser,modifyName,modifyNumber,mosifyEmail))
            }

            3 -> {  print("Introdueix l'ID de l'usuari: ")
                val idUser = readLine()!!
                blockAndUnlockUser(idUser)
            }
        }
        if (userNumber !in 1..4){
            println("$redColor \bNúmero no vàlid\n$reset")
        }
    }while (userNumber != 4)
    backup()
}
fun gymMenu(){
    println("❚█══█❚\n" +
            "1 -> Crear usuari\n" +
            "2 -> Modificar usuari\n" +
            "3 -> Des/Bloquejar usuari\n" +
            "4 -> Sortir\n" +
            "❚█══█❚\n")
}
fun importFiles() {
    val path = Path  ("src/main/Import/")
    val filesOfDirectory: List<Path> = path.listDirectoryEntries()
    if (filesOfDirectory.isNotEmpty()) {
        readFiles(path)
    }
}
fun readFiles(path: Path) {
    val fileUserData= File("src/main/Data/UserData").readLines()
    val fileOld = File("src/main/Import/OldSystem")
    var lastId = fileUserData.size

    for (file in path.listDirectoryEntries()) {
        val linesOfFile = file.readLines()

        for (line in linesOfFile) {
            var comprovation = true
            val userInfo = line.replace(";", ",").split(",").toMutableList()

            if (userInfo[4] == "false" && userInfo[5] == "true") {
                comprovation = false
            }
            if(comprovation){
                lastId++
                userInfo[0] = lastId.toString()
                newDataPath.appendText(userInfo.toString().replace("[", "").replace("]", "").replace(",", ";"))
                newDataPath.appendText("\n")
            }
        }
    }
    fileOld.delete()

}

fun createUser(nom: String, number: String, email:String):String{
    val fileUserData= File("src/main/Data/UserData").readLines()
    var newUser =""
    val lastId = fileUserData.size + 1

    newUser += "$lastId; $nom; $number; $email; true; false;\n"
    newDataPath.appendText(newUser)
    return "$greenColor \bUsuari creat satisfactoriament\n$reset"
}

fun modifyUser(idUser: String, modifyName: String, modifyNumber: String, modifyEmail: String) : String{


    val pathUserData = Path("src/main/Data/UserData")
    val userData = pathUserData.readLines()

    if (idUser.toInt() in 1 .. userData.size) {

        pathUserData.writeText("")

        for (i in 1 .. userData.size) {
            if (i == idUser.toInt()) {
                pathUserData.appendText("$idUser; $modifyName; $modifyNumber; $modifyEmail; true; false; \n")
            }
            else pathUserData.appendText("${userData[i-1]}\n")
        }
        return "$greenColor \bUsuari modificat satisfactoriament\n$reset"
    }
    return "$redColor \bID no vàlida\n$reset"
}
fun blockAndUnlockUser(idUser :String)  {
    val fileUserData = Path("src/main/Data/UserData")
    val userData = fileUserData.readLines()

    if (idUser.toInt() in 1 ..  userData.size) {
       fileUserData.writeText("")

        for (i in 1 .. userData.size) {
            if (i == idUser.toInt()) {
                val userInfo = userData[idUser.toInt()-1].split("; ").toMutableList()

                val block = userInfo[5].replace(";","").toBoolean()

                if(block){
                    println("Usuari bloquejat. Vols desbloquejar-lo? SI / NO")

                    val answerUser = scanner.next().uppercase()

                    if(answerUser== "SI"){
                        fileUserData.appendText("${userInfo[0]}; ${userInfo[1]}; ${userInfo[2]}; ${userInfo[3]}; ${userInfo[4]}; ${!userInfo[5].toBoolean()}; \n")
                        println("$greenColor \bUsuari \"${userInfo[1]}\" desbloquejat\n$reset")
                    }
                    else{
                        fileUserData.appendText("${userInfo[0]}; ${userInfo[1]}; ${userInfo[2]}; ${userInfo[3]}; ${userInfo[4]}; ${userInfo[5]}; \n")
                        println("$greenColor \bL'usuari \"${userInfo[1]}\" continua bloquejat\n$reset")
                    }
                }
                else{
                    println("Usuari desbloquejat. Vols bloquejar-lo? SI / NO")
                    val answerUser = scanner.next().uppercase()

                    if (answerUser== "SI"){
                        fileUserData.appendText("${userInfo[0]}; ${userInfo[1]}; ${userInfo[2]}; ${userInfo[3]}; ${userInfo[4]}; ${!userInfo[5].toBoolean()}\n")
                        println("$greenColor \bUsuari \"${userInfo[1]}\" bloquejat\n$reset")
                    }
                    else{
                        fileUserData.appendText("${userInfo[0]}; ${userInfo[1]}; ${userInfo[2]}; ${userInfo[3]}; ${userInfo[4]}; ${userInfo[5]};\n")
                        println("$greenColor \bL'usuari \"${userInfo[1]}\" continua desbloquejat\n$reset")
                    }
                }
            }
            else{
                fileUserData.appendText("${userData[i-1]}\n")
            }
        }
    }
    else println("$redColor \bID no vàlida\n$reset")

}
fun backup(){

    val fileUserData = File("src/main/Data/UserData")

    val formatter = SimpleDateFormat("dd-MM-yyyy")
    val day = Date()
    val today = formatter.format(day)


    val backup = File("src/main/Backups/$today\bUserData.txt")

    if (backup.exists()) {
        backup.delete()
        fileUserData.copyTo(backup)

        println("$greenColor \bBackup $today actualitzada\n$reset")
    }
    else {
       fileUserData.copyTo(backup)
        println("$greenColor \bBackup $today creada\n$reset")
    }

}

















